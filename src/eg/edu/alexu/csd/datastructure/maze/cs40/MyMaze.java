package eg.edu.alexu.csd.datastructure.maze.cs40;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import eg.edu.alexu.csd.datastructure.maze.IMazeSolver;
import eg.edu.alexu.csd.datastructure.queue.cs40.MyQueueLinkedBased;
import eg.edu.alexu.csd.datastructure.stack.cs40.MyStack;

public class MyMaze implements IMazeSolver {

	public int[][] solveBFS(File maze) {

		BufferedReader bufferedReader = null;

		try {

			FileReader fileReader = new FileReader(maze);
			bufferedReader = new BufferedReader(fileReader);
			String line = bufferedReader.readLine();

			line = line.trim();
			int i, rows, cols;

			StringBuilder currentNumber = new StringBuilder();
			for (i = 0; i < line.length(); ++i) {
				if (!Character.isWhitespace(line.charAt(i))) {
					currentNumber.append(line.charAt(i));
				} else {
					++i;
					break;
				}
			}
			rows = Integer.parseInt(currentNumber.toString());

			currentNumber = new StringBuilder();
			for (; i < line.length(); ++i) {
				if (!Character.isWhitespace(line.charAt(i))) {
					currentNumber.append(line.charAt(i));
				} else {
					break;
				}
			}
			cols = Integer.parseInt(currentNumber.toString());

			char[][] mazeContent = new char[rows][cols];
			boolean[][] isVisited = new boolean[rows][cols];
			Point[][] parents = new Point[rows][cols];

			Point start = null;
			Point end = null;

			for (int x = 0; x < rows; ++x) {
				for (int y = 0; y < cols; ++y) {
					mazeContent[x][y] = (char) bufferedReader.read();
					isVisited[x][y] = false;
					parents[x][y] = null;

					if (mazeContent[x][y] == 'S') {
						if (start == null) {
							start = new Point(x, y);
						} else {
							throw new RuntimeException("more than one start");
						}
					} else if (mazeContent[x][y] == 'E') {
						if (end == null) {
							end = new Point(x, y);
						} else {
							throw new RuntimeException("more than one end");
						}
					} else if (mazeContent[x][y] != '.' && mazeContent[x][y] != '#') {
						throw new RuntimeException("invalid cell: " + mazeContent[x][y]);
					}
				}
				
				bufferedReader.readLine();
			}
			
			if(end == null) {
				throw new RuntimeException("no end point");
			}

			MyQueueLinkedBased q = new MyQueueLinkedBased();

			q.enqueue(start);
			isVisited[start.x][start.y] = true;
			Point current, top, bottom, left, right;
			current = null;
			boolean solved = false;

			while (!q.isEmpty()) {
				current = (Point) q.dequeue();
				isVisited[current.x][current.y] = true;

				if (current.equals(end)) {
					solved = true;
					break;
				}
				top = new Point(current.x - 1, current.y);
				bottom = new Point(current.x + 1, current.y);
				left = new Point(current.x, current.y - 1);
				right = new Point(current.x, current.y + 1);

				
				
				
				if (right.y < cols) {
					if (!isVisited[right.x][right.y] && mazeContent[right.x][right.y] != '#') {
						parents[right.x][right.y] = current;
						q.enqueue(right);
					}
				}
				if (left.y >= 0) {
					if (!isVisited[left.x][left.y] && mazeContent[left.x][left.y] != '#') {
						parents[left.x][left.y] = current;
						q.enqueue(left);
					}
				}
				if (bottom.x < rows) {
					if (!isVisited[bottom.x][bottom.y] && mazeContent[bottom.x][bottom.y] != '#') {
						parents[bottom.x][bottom.y] = current;
						q.enqueue(bottom);
					}
				}
				if (top.x >= 0) {
					if (!isVisited[top.x][top.y] && mazeContent[top.x][top.y] != '#') {
						parents[top.x][top.y] = current;
						q.enqueue(top);
					}
				}
				

				

				

			}

			MyStack path = new MyStack();

			if (solved) {
				while (current != null) {
					path.push(current);
					current = parents[current.x][current.y];
				}

				int[][] pathArray = new int[path.size()][2];
				i = 0;
				while (!path.isEmpty()) {
					current = (Point) path.pop();
					pathArray[i][0] = current.x;
					pathArray[i][1] = current.y;
					++i;
				}

				return pathArray;
			} else {
				return null;
			}

		} catch (Exception e) {
			throw new RuntimeException(e.toString());
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					throw new RuntimeException(e.toString());
				}
			}
		}

	}

	public int[][] solveDFS(File maze) {

		BufferedReader bufferedReader = null;

		try {
			FileReader fileReader = new FileReader(maze);
			bufferedReader = new BufferedReader(fileReader);
			String line = bufferedReader.readLine();

			line = line.trim();
			int i, rows, cols;

			StringBuilder currentNumber = new StringBuilder();
			for (i = 0; i < line.length(); ++i) {
				if (!Character.isWhitespace(line.charAt(i))) {
					currentNumber.append(line.charAt(i));
				} else {
					++i;
					break;
				}
			}
			rows = Integer.parseInt(currentNumber.toString());

			currentNumber = new StringBuilder();
			for (; i < line.length(); ++i) {
				if (!Character.isWhitespace(line.charAt(i))) {
					currentNumber.append(line.charAt(i));
				} else {
					break;
				}
			}
			cols = Integer.parseInt(currentNumber.toString());

			char[][] mazeContent = new char[rows][cols];
			boolean[][] isVisited = new boolean[rows][cols];
			Point[][] parents = new Point[rows][cols];

			Point start = null;
			Point end = null;

			for (int x = 0; x < rows; ++x) {
				for (int y = 0; y < cols; ++y) {
					mazeContent[x][y] = (char) bufferedReader.read();
					isVisited[x][y] = false;
					parents[x][y] = null;

					if (mazeContent[x][y] == 'S') {
						if (start == null) {
							start = new Point(x, y);
						} else {
							throw new RuntimeException("more than one start");
						}
					} else if (mazeContent[x][y] == 'E') {
						if (end == null) {
							end = new Point(x, y);
						} else {
							throw new RuntimeException("more than one end");
						}
					} else if (mazeContent[x][y] != '.' && mazeContent[x][y] != '#') {
						throw new RuntimeException("invalid cell");
					}
				}
				bufferedReader.readLine();
			}
			
			if(end == null) {
				throw new RuntimeException("no end point");
			}

			MyStack q = new MyStack();

			q.push(start);
			isVisited[start.x][start.y] = true;
			Point current, top, bottom, left, right;
			current = null;
			boolean solved = false;

			while (!q.isEmpty()) {
				current = (Point) q.pop();
				isVisited[current.x][current.y] = true;

				if (current.equals(end)) {
					solved = true;
					break;
				}
				top = new Point(current.x - 1, current.y);
				bottom = new Point(current.x + 1, current.y);
				left = new Point(current.x, current.y - 1);
				right = new Point(current.x, current.y + 1);

				if (right.y < cols) {
					if (!isVisited[right.x][right.y] && mazeContent[right.x][right.y] != '#') {
						parents[right.x][right.y] = current;
						q.push(right);
					}
				}
				if (left.y >= 0) {
					if (!isVisited[left.x][left.y] && mazeContent[left.x][left.y] != '#') {
						parents[left.x][left.y] = current;
						q.push(left);
					}
				}

				if (bottom.x < rows) {
					if (!isVisited[bottom.x][bottom.y] && mazeContent[bottom.x][bottom.y] != '#') {
						parents[bottom.x][bottom.y] = current;
						q.push(bottom);
					}
				}

				if (top.x >= 0) {
					if (!isVisited[top.x][top.y] && mazeContent[top.x][top.y] != '#') {
						parents[top.x][top.y] = current;
						q.push(top);
					}
				}

			}

			MyStack path = new MyStack();

			if (solved) {
				while (current != null) {
					path.push(current);
					current = parents[current.x][current.y];
				}

				int[][] pathArray = new int[path.size()][2];
				i = 0;
				while (!path.isEmpty()) {
					current = (Point) path.pop();
					pathArray[i][0] = current.x;
					pathArray[i][1] = current.y;
					++i;
				}

				return pathArray;
			} else {
				return null;
			}

		} catch (Exception e) {
			throw new RuntimeException(e.toString());
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					throw new RuntimeException(e.toString());
				}
			}
		}

	}
}
