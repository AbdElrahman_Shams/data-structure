package eg.edu.alexu.csd.datastructure.calculator.cs40;

import java.util.Scanner;

import eg.edu.alexu.csd.datastructure.calculator.ICalculator;

public class Test 
{
	public static void main(String[] args) 
	{
		ICalculator cal = new MyCalculator();
	    Scanner getInteger = new Scanner(System.in);
		int x,y;
		x = getInteger.nextInt();
		y = getInteger.nextInt();
		System.out.println(cal.add(x,y));
		System.out.println(cal.divide(x,y));	
	}
}