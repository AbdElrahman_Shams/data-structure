package eg.edu.alexu.csd.datastructure.stack.cs40;

import java.util.EmptyStackException;

import eg.edu.alexu.csd.datastructure.linkedList.cs34_40.SinglyLinkedList;
import eg.edu.alexu.csd.datastructure.stack.IStack;

public class MyStack implements IStack {

	private SinglyLinkedList base = new SinglyLinkedList();

	public void add(int index, Object element) {

		if ((index < 0) || (index > this.size())) {
			throw new IndexOutOfBoundsException();
		} else {

			base.add(base.size() - index, element);
		}

	}

	public Object pop() {
		if (this.isEmpty()) {
			throw new EmptyStackException();
		}
		Object top;
		top = base.get(0);
		base.remove(0);
		return top;
	}

	public Object peek() {
		if (this.isEmpty()) {
			throw new EmptyStackException();
		}
		Object top;
		top = base.get(0);
		return top;
	}

	public void push(Object element) {
		base.add(0, element);
	}

	public boolean isEmpty() {
		return base.isEmpty();
	}

	public int size() {
		return base.size();
	}

}
