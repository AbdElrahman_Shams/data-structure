package eg.edu.alexu.csd.datastructure.stack.cs40;

import eg.edu.alexu.csd.datastructure.stack.IExpressionEvaluator;

public class MyExpressionEvaluator implements IExpressionEvaluator {

	private static boolean isOperator(char c) {
		return c == '+' || c == '-' || c == '*' || c == '/' || c == '^' || c == '(' || c == ')';
	}

	private static boolean isOperatorr(char c) {
		return c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
	}

	private static boolean isLowerPrecedence(char op1, char op2) {
		switch (op1) {
		case '+':
		case '-':
			return !(op2 == '+' || op2 == '-');

		case '*':
		case '/':
			return op2 == '^' || op2 == '(';

		case '^':
			return op2 == '(';

		case '(':
			return true;

		default:
			return false;
		}
	}

	public String infixToPostfix(String expression) {

		if (expression == null || expression.length() == 0) {
			throw null;
		}

		else {

			char c;
			for (int j = 1; j < expression.length() - 1; j++) {

				c = expression.charAt(j);

				if (isOperatorr(c) && isOperatorr(expression.charAt(j - 1))) {
					throw null;
				}
				if (!(!isOperator(expression.charAt(j - 1)) && isOperatorr(c) && !isOperator(expression.charAt(j + 1)))) {
					throw null;
				}

			}
			MyStack stack = new MyStack();
			String infix = new String(expression);
			StringBuffer postfix = new StringBuffer(infix.length());

			for (int i = 0; i < infix.length(); i++) {
				c = infix.charAt(i);

				if (!isOperator(c)) {
					postfix.append(c);
				}

				else {
					if (c == ')') {

						while (!stack.isEmpty() && (char) stack.peek() != '(') {
							postfix.append(stack.pop());
						}
						if (!stack.isEmpty()) {
							stack.pop();
						}
					}

					else {
						if (!stack.isEmpty() && !isLowerPrecedence(c, (char) stack.peek())) {
							stack.push(c);
						} else {
							while (!stack.isEmpty() && isLowerPrecedence(c, (char) stack.peek())) {
								Character pop = (char) stack.pop();
								if (c != '(') {
									postfix.append(pop);
								} else {
									c = pop;
								}
							}
							stack.push(c);
						}

					}
				}
			}
			while (!stack.isEmpty()) {
				postfix.append(stack.pop());
			}
			return postfix.toString();
		}
	}

	public int evaluate(String expression) {

		if (expression == null || expression.length() == 0) {
			throw null;
		}
		char c;
		for (int j = 1; j < expression.length() - 1; j++) {

			c = expression.charAt(j);

			if (isOperatorr(c) && isOperatorr(expression.charAt(j - 1))) {
				throw null;
			}
			if (!(!isOperator(expression.charAt(j - 1)) && isOperatorr(c) && !isOperator(expression.charAt(j + 1)))){
				throw null;
			}

		}
		return 0;
	}
}
