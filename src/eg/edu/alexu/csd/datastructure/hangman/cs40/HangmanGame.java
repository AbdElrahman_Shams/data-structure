package eg.edu.alexu.csd.datastructure.hangman.cs40;
 
import java.util.Random;

import eg.edu.alexu.csd.datastructure.hangman.IHangman;


public class HangmanGame implements IHangman
{
	
	private String[] words;
	private String secretWord;
	private String word;
	private int maxNumOfWrongGuesses;
	private int numOfGuesses = 0;
	
	/**
	*Set dictionary words to pick secret words from
	* @param words an array of words
	*/
	public void setDictionary(String[] words) 
	{
		this.words = words;
	}
	/**
	* Pick a random secret word from dictionary and returns it
	* @return secret word
	*/
	public String selectRandomSecretWord()
	{	
		char[] charArray = null;
		Random rand = new Random();
		if(words == null)
			return null;
		int x = rand.nextInt(words.length);
		this.secretWord = this.words[x];
		this.word = this.secretWord;
		charArray = word.toCharArray();
		for(int i = 0; i<this.secretWord.length(); ++i)
		{
			charArray[i] = '-';
		}
		
		this.word = new String(charArray);
		
		return this.secretWord;
	}
	/**
	* Receive a new user guess, and verify it against the secret word.
	* @param c
	* case insensitive user guess.
	* If c is NULL then ignore it and do no change
	* @return
	* secret word with hidden characters (use '-' instead unsolved
	* characters), or return NULL if user reached max wrong guesses
	*/
	public String guess(Character c)
	{
		
		String s = this.secretWord.toLowerCase();
		c = Character.toLowerCase(c);
		char[] hiddenWord = null;
		
		if(numOfGuesses==maxNumOfWrongGuesses)
		{
			return null;
		}
		
		else if(!s.contains(c.toString(c)))
		{
			this.numOfGuesses++;
			if(numOfGuesses==maxNumOfWrongGuesses)
			{
				return null;
			}
			
			return this.word;
		}
		
		else
		{
			hiddenWord = this.word.toCharArray();
			
			for(int i = 0;i<this.secretWord.length();++i)
			{
				if(c==s.charAt(i))
				{
					hiddenWord[i] = secretWord.charAt(i); 
				}
			}
			this.word = new String(hiddenWord);
			
		} 
		
		return this.word;
	}
	/***
	* Set the maximum number of wrong guesses
	* @param max
	* maximum number of wrong guesses, If is NULL, then assume it 0 
	*/
	public void setMaxWrongGuesses(Integer max)
	{
		this.maxNumOfWrongGuesses = max;
	}

}