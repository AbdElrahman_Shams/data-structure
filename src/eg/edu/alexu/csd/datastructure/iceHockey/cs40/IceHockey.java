
package eg.edu.alexu.csd.datastructure.iceHockey.cs40;

import java.awt.Point;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Vector;

import eg.edu.alexu.csd.datastructure.iceHockey.IPlayersFinder;

public class IceHockey implements IPlayersFinder
{
	
	public IceHockey()
	{
		this.allVisitedPixels = new Vector<Point>();
		this.visitedPixels = new Vector<Point>();
	}
	
	private Vector<Point> allVisitedPixels;
	private Vector<Point> visitedPixels;
	
	private Vector<Point> findPixels(String[] photo,int team, Point p)
	{
		int i = p.y;
		int j = p.x;
		int n = photo.length;
		int m = photo[0].length();
		
		if(!(i>=0 && j>=0 && i<n && j<m)||(visitedPixels.contains(p))||photo[i].charAt(j) != '0'+team )
		{
			return visitedPixels;
		}
		else
		{
			allVisitedPixels.add(p);
			visitedPixels.add(p);
			findPixels(photo, team, new Point(p.x+1,p.y));
			findPixels(photo, team, new Point(p.x-1,p.y));
			findPixels(photo, team, new Point(p.x,p.y+1));
			findPixels(photo, team, new Point(p.x,p.y-1));
			return visitedPixels;
		}
		
	}	
	
	private static class XComparator implements Comparator<Point> 
	{
		public int compare(Point a, Point b) 
		{
			return Integer.compare(a.x, b.x);
		}
	}
	
	private static class YComparator implements Comparator<Point> 
	{
		public int compare(Point a, Point b) 
		{
			return Integer.compare(a.y, b.y);
		}
	}
	
	private Point calculateCenter(Vector<Point> positionsOfPixels)
	{
		Point[] pixels = new Point[positionsOfPixels.size()];
		positionsOfPixels.toArray(pixels);
		
		Arrays.sort(pixels,new XComparator());
		Point[] xBorders = new Point[2];
		xBorders[0] = pixels[0];
		xBorders[1] = pixels[pixels.length-1];
		
		Arrays.sort(pixels,new YComparator());
		Point[] yBorders = new Point[2];
		yBorders[0] = pixels[0];
		yBorders[1] = pixels[pixels.length-1];
		
		int x = xBorders[0].x + xBorders[1].x + 1;
		int y = yBorders[0].y + yBorders[1].y + 1;
		
		return new Point(x,y);
	}
	
	private boolean isVisited(Point p) 
	{
		return allVisitedPixels.contains(p);
	}
	
	public Point[] findPlayers(String[] photo, int team, int threshold) 
	{
	
		Vector<Point> players = new Vector<Point>();		

		for(int i = 0; i<photo.length; ++i)
		{
			for(int j = 0; j<photo[i].length(); ++j)
			{
				if ((photo[i].charAt(j) == '0'+team)&&(!isVisited(new Point(j,i))))
				{
					visitedPixels = findPixels(photo, team, new Point(j,i));
					int area = visitedPixels.size()*4;
					if (area>=threshold)
					{
						players.add(calculateCenter(visitedPixels));
					}
					visitedPixels = new Vector<Point>();
				}
			}
		}
		
		Point[] playersArr = new Point[players.size()];
		players.toArray(playersArr);
		Arrays.sort(playersArr,new PlayersComparator());
		
		return playersArr;
	}
	
	private static class PlayersComparator implements Comparator<Point> 
	{
		public int compare(Point a, Point b) 
		{
			if(a.x == b.x) 
			{
				return Integer.compare(a.y, b.y);
			} 
			else 
			{
				return Integer.compare(a.x, b.x);
			}
		}
	}
}
