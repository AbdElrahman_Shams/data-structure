package eg.edu.alexu.csd.datastructure.queue.cs40;

import eg.edu.alexu.csd.datastructure.queue.IArrayBased;
import eg.edu.alexu.csd.datastructure.queue.IQueue;

public class MyQueueArrayBased implements IArrayBased,IQueue {

	private int f = 0;
	private int r = 0;
	private Object[] baseArray;

	public MyQueueArrayBased(int n) {

		baseArray = new Object[n];
	}

	public void enqueue(Object item) {
		
		if(r == baseArray.length){
			throw new RuntimeException();
		}
		baseArray[r] = item;
		++r;

	}

	public Object dequeue() {

		if (isEmpty()) {
			throw new RuntimeException();
		}
		Object front = baseArray[f];
		++f;
		return front;
	}

	public boolean isEmpty() {
		if (size() == 0) {
			return true;
		}
		return false;
	}

	public int size() {

		return r - f;
	}

}
