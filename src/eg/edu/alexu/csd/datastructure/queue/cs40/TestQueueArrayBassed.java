package eg.edu.alexu.csd.datastructure.queue.cs40;

import org.junit.*;
import static org.junit.Assert.*;

public class TestQueueArrayBassed {

	@Test
	public void testEnqueue() {
		MyQueueArrayBased q = new MyQueueArrayBased(5);

		q.enqueue(1);
		q.enqueue(2);
		assertEquals(1, q.dequeue());
		q.enqueue(3);
		assertEquals(2, q.dequeue());
		q.enqueue(4);
		assertEquals(3, q.dequeue());
		q.enqueue(5);
		assertEquals(4, q.dequeue());
		assertEquals(5, q.dequeue());
	}

	@Test(expected = RuntimeException.class)
	public void testEmptyQueue() {
		MyQueueArrayBased q = new MyQueueArrayBased(5);
		q.dequeue();
	}

	@Test(expected = RuntimeException.class)
	public void testFullQueue() {
		MyQueueArrayBased q = new MyQueueArrayBased(3);
		q.enqueue(1);
		q.enqueue(2);
		assertEquals(1, q.dequeue());
		q.enqueue(3);
		assertEquals(2, q.dequeue());
		q.enqueue(4);
	}

	@Test
	public void testSize() {
		MyQueueArrayBased q = new MyQueueArrayBased(10);

		assertEquals(0, q.size());
		q.enqueue(1);
		q.enqueue(2);
		q.dequeue();
		q.dequeue();
		assertEquals(0, q.size());
		q.enqueue(3);
		q.dequeue();
		q.enqueue(4);
		q.dequeue();
		q.enqueue(5);
		assertEquals(1, q.size());
		q.enqueue(6);
		q.dequeue();
		q.enqueue(7);
		q.dequeue();
		q.enqueue(8);
		assertEquals(2, q.size());
		q.dequeue();
		q.enqueue(9);
		q.dequeue();
		q.dequeue();
		q.enqueue(10);
		assertEquals(1, q.size());
	}
	
	@Test
	public void testisEmpty(){
		MyQueueArrayBased q = new MyQueueArrayBased(5);
		
		assertEquals(true, q.isEmpty());
		q.enqueue(1);
		q.enqueue(2);
		q.dequeue();
		q.dequeue();
		assertEquals(true, q.isEmpty());
		q.enqueue(3);
		q.dequeue();
		q.enqueue(4);
		assertEquals(false, q.isEmpty());
		
	}

}
