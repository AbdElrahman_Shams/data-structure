package eg.edu.alexu.csd.datastructure.queue.cs40;

import eg.edu.alexu.csd.datastructure.linkedList.cs34_40.SinglyLinkedList;
import eg.edu.alexu.csd.datastructure.queue.ILinkedBased;
import eg.edu.alexu.csd.datastructure.queue.IQueue;

public class MyQueueLinkedBased implements ILinkedBased, IQueue {

	private SinglyLinkedList base = new SinglyLinkedList();

	public void enqueue(Object item) {

		base.add(item);
	}

	public Object dequeue() {

		if (isEmpty()) {
			throw new RuntimeException();
		}
		Object front = new Object();
		front = base.get(0);
		base.remove(0);
		return front;
	}

	public boolean isEmpty() {
		return base.isEmpty();
	}

	public int size() {
		return base.size();
	}
}
