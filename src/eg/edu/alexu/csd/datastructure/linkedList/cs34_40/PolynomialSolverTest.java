package eg.edu.alexu.csd.datastructure.linkedList.cs34_40;

import org.junit.*;
import static org.junit.Assert.*;

public class PolynomialSolverTest {

	@Test
	public final void testSetPolynomial() {
		PolynomialSolver poly = new PolynomialSolver();

		poly.setPolynomial('A', new int[][] { { -5, 3 }, { 3, 2 }, { 1, 1 }, { 9, 0 } });
		assertEquals("-5x^3+3x^2+x+9", poly.print('A'));

		poly.setPolynomial('B', new int[][] { { 8, 4 }, { 2, 2 }, { -5, 0 } });
		assertEquals("8x^4+2x^2-5", poly.print('B'));

		poly.setPolynomial('C', new int[][] { { 0, 5 }, { 0, 4 }, { 1, 3 }, { 0, 3 } });
		assertEquals("x^3", poly.print('C'));

		poly.clearPolynomial('A');
		poly.setPolynomial('A', new int[][] { { 1, 2 } });
		assertEquals("x^2", poly.print('A'));

		poly.clearPolynomial('A');
		poly.setPolynomial('A', new int[][] { { 0, 0 } });
		assertEquals("0", poly.print('A'));

		poly.clearPolynomial('A');
		poly.setPolynomial('A', new int[][] { { 4, 0 } });
		assertEquals("4", poly.print('A'));
	}

	@Test
	public void testClearPolynomial() {
		PolynomialSolver poly = new PolynomialSolver();
		poly.setPolynomial('A', new int[][] { { 3, 4 }, { 1, 2 } });
		poly.clearPolynomial('A');
		poly.setPolynomial('A', new int[][] { { 100, 5 }, { 200, 3 } });
		assertEquals("100x^5+200x^3", poly.print('A'));
		poly.clearPolynomial('B');
		poly.clearPolynomial('A');
		poly.clearPolynomial('A');
	}

	@Test
	public void testEvaluatePolynomial() {
		PolynomialSolver poly = new PolynomialSolver();
		poly.setPolynomial('A', new int[][] { { -5, 3 }, { 3, 2 }, { 1, 1 }, { 9, 0 } });
		poly.setPolynomial('B', new int[][] { { 8, 4 }, { 2, 2 }, { 5, 0 } });
		poly.setPolynomial('C', new int[][] { { 0, 5 }, { 0, 4 }, { 1, 3 }, { 0, 3 } });

		assertEquals(8, poly.evaluatePolynomial('A', 1), 1e-9);
		assertEquals(9, poly.evaluatePolynomial('A', 0), 1e-9);
		assertEquals(671, poly.evaluatePolynomial('B', -3), 1e-9);
		assertEquals(-125, poly.evaluatePolynomial('C', -5), 1e-9);
	}

	@Test
	public void testAdd() {
		PolynomialSolver poly = new PolynomialSolver();
		poly.setPolynomial('A', new int[][] { { 9, 5 }, { 3, 4 } });
		poly.setPolynomial('B', new int[][] { { 3, 6 }, { -3, 4 } });
		poly.setPolynomial('C', new int[][] { { 1, 6 }, { 3, 3 }, { 2, 2 }, { 1, 1 } });

		assertArrayEquals(new int[][] { { 3, 6 }, { 9, 5 } }, poly.add('A', 'B'));
		assertEquals("3x^6+9x^5", poly.print('R'));

		assertArrayEquals(new int[][] { { 3, 6 }, { 9, 5 } }, poly.add('B', 'A'));
		assertEquals("3x^6+9x^5", poly.print('R'));

		assertArrayEquals(new int[][] { { 1, 6 }, { 9, 5 }, { 3, 4 }, { 3, 3 }, { 2, 2 }, { 1, 1 } },
				poly.add('A', 'C'));
		assertEquals("x^6+9x^5+3x^4+3x^3+2x^2+x", poly.print('R'));

		poly.setPolynomial('B', new int[][] { { 0, 0 } });
		assertArrayEquals(new int[][] { { 9, 5 }, { 3, 4 } }, poly.add('A', 'B'));
		assertEquals("9x^5+3x^4", poly.print('R'));

		poly.setPolynomial('B', new int[][] { { -9, 5 }, { -3, 4 } });
		assertArrayEquals(new int[][] { { 0, 0 } }, poly.add('A', 'B'));
		assertEquals("0", poly.print('R'));
	}

	@Test
	public void testSubtract() {
		PolynomialSolver poly = new PolynomialSolver();
		poly.setPolynomial('A', new int[][] { { 9, 5 }, { 3, 4 } });
		poly.setPolynomial('B', new int[][] { { 3, 6 }, { -3, 4 } });
		poly.setPolynomial('C', new int[][] { { 1, 6 }, { 3, 3 }, { 2, 2 }, { 1, 1 } });

		assertArrayEquals(new int[][] { { 0, 0 } }, poly.subtract('A', 'A'));
		assertEquals("0", poly.print('R'));

		assertArrayEquals(new int[][] { { 3, 6 }, { -9, 5 }, { -6, 4 } }, poly.subtract('B', 'A'));
		assertEquals("3x^6-9x^5-6x^4", poly.print('R'));

		assertArrayEquals(new int[][] { { -1, 6 }, { 9, 5 }, { 3, 4 }, { -3, 3 }, { -2, 2 }, { -1, 1 } },
				poly.subtract('A', 'C'));
		assertEquals("-x^6+9x^5+3x^4-3x^3-2x^2-x", poly.print('R'));

		poly.setPolynomial('B', new int[][] { { 0, 0 } });
		assertArrayEquals(new int[][] { { 9, 5 }, { 3, 4 } }, poly.subtract('A', 'B'));
		assertEquals("9x^5+3x^4", poly.print('R'));
	}

	@Test
	public void testMultiply() {
		PolynomialSolver poly = new PolynomialSolver();
		poly.setPolynomial('A', new int[][] { { 3, 7 }, { 1, 2 } });
		poly.setPolynomial('B', new int[][] { { 3, 7 }, { 1, 2 } });
		poly.setPolynomial('C', new int[][] { { 1, 0 } });

		assertArrayEquals(new int[][] { { 9, 14 }, { 6, 9 }, { 1, 4 } }, poly.multiply('A', 'B'));
		assertEquals("9x^14+6x^9+x^4", poly.print('R'));

		assertArrayEquals(new int[][] { { 3, 7 }, { 1, 2 } }, poly.multiply('A', 'C'));
		assertEquals("3x^7+x^2", poly.print('R'));

		poly.setPolynomial('C', new int[][] { { 0, 0 } });
		assertArrayEquals(new int[][] { { 0, 0 } }, poly.multiply('A', 'C'));
		assertEquals("0", poly.print('R'));
	}

}

