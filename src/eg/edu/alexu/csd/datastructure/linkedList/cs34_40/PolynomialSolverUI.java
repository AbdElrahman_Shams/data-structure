package eg.edu.alexu.csd.datastructure.linkedList.cs34_40;

import java.io.BufferedInputStream;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class PolynomialSolverUI {
	public static void main(String[] args) {
		PolynomialSolver ps = new PolynomialSolver();
		Scanner sc = new Scanner(new BufferedInputStream(System.in));
		String choice = new String();
		String currentLine = new String();
		ArrayList<int[]> inputs = null;
		String poly1 = null;
		String poly2 = null;

		do {
			System.out.println("Please choose an action");
			System.out.println("-----------------------");
			System.out.println("1: Set a polynomial variable");
			System.out.println("2: Print the value of a polynomial variable");
			System.out.println("3: Add two polynomials");
			System.out.println("4: Subtract two polynomials");
			System.out.println("5: Multiply two polynomials");
			System.out.println("6: Evaluate a polynomial at some point");
			System.out.println("7: Clear a polynomial variable");
			System.out.println("To exit, type 'exit' (without the quotes) and press ENTER");
			System.out.println("====================================================================");

			choice = sc.nextLine();
			try {
				switch (choice) {
				case "1":
					System.out.println("Insert  the  variable  name: A, B or C");
					poly1 = sc.nextLine();
					switch (poly1) {
					case "A":
					case "B":
					case "C":
						inputs = new ArrayList<int[]>();
						System.out.println("Insert the polynomial terms in the form:");
						System.out.println("(coeff1 , exponent1), (coeff2 , exponent2), ..");
						currentLine = sc.nextLine();
						if (!Pattern.matches(
								"(\\s*\\(\\s*[-+]?\\d+\\s*,\\s*[-+]?\\d+\\s*\\)){1}" +
								"(\\s*,\\s*\\(\\s*[-+]?\\d+\\s*,\\s*[-+]?\\d+\\s*\\))*",
								currentLine)) {
							System.out.println("Invalid format!");
						} else {
							Scanner lineScanner = new Scanner(currentLine);
							String currentPattern = lineScanner.findInLine("[-+]?\\d+");
							while (currentPattern != null) {
								int[] term = new int[2];
								term[0] = Integer.parseInt(currentPattern); 
								currentPattern = lineScanner.findInLine("[-+]?\\d+");
								term[1] = Integer.parseInt(currentPattern); 
								inputs.add(term);
								currentPattern = lineScanner.findInLine("[-+]?\\d+");
							}

							lineScanner.close();
							ps.setPolynomial(poly1.charAt(0), inputs.toArray(new int[inputs.size()][2]));
							System.out.println("Polynomial " + poly1.charAt(0) + " is set");
						}
						System.out.println();
						break;
					default:
						System.out.println("Unknown variable!");
						System.out.println();
					}
					break;
				case "2":
					System.out.println("Insert  the  variable  name: A, B, C or R");
					poly1 = sc.nextLine();
					switch (poly1) {
					case "A":
					case "B":
					case "C":
					case "R":
						if (ps.isSet(poly1.charAt(0))) {
							System.out.println(ps.print(poly1.charAt(0)));
						} else {
							System.out.println("Variable not set");
						}
						System.out.println();
						break;
					default:
						System.out.println("Unknown variable!");
						System.out.println();
					}
					break;
				case "3":
					System.out.println("Insert  first  operand  variable  name: A, B or C");
					poly1 = sc.nextLine();
					switch (poly1) {
					case "A":
					case "B":
					case "C":
						if (!ps.isSet(poly1.charAt(0))) {
							System.out.println("Variable not set");
						} else {
							System.out.println("Insert  second  operand  variable  name: A, B or C");
							poly2 = sc.nextLine();
							switch (poly2) {
							case "A":
							case "B":
							case "C":
								if (!ps.isSet(poly2.charAt(0))) {
									System.out.println("Variable not set");
								} else {
									ps.add(poly1.charAt(0), poly2.charAt(0));
									System.out.println("Result set in R: " + ps.print('R'));
								}
								break;
							default:
								System.out.println("Unknown variable!");
								System.out.println();
							}
						}
						System.out.println();
						break;
					default:
						System.out.println("Unknown variable!");
						System.out.println();
					}
					break;
				case "4":
					System.out.println("Insert  first  operand  variable  name: A, B or C");
					poly1 = sc.nextLine();
					switch (poly1) {
					case "A":
					case "B":
					case "C":
						if (!ps.isSet(poly1.charAt(0))) {
							System.out.println("Variable not set");
						} else {
							System.out.println("Insert  second  operand  variable  name: A, B or C");
							poly2 = sc.nextLine();
							switch (poly2) {
							case "A":
							case "B":
							case "C":
								if (!ps.isSet(poly2.charAt(0))) {
									System.out.println("Variable not set");
								} else {
									ps.subtract(poly1.charAt(0), poly2.charAt(0));
									System.out.println("Result set in R: " + ps.print('R'));
								}
								break;
							default:
								System.out.println("Unknown variable!");
								System.out.println();
							}
						}
						System.out.println();
						break;
					default:
						System.out.println("Unknown variable!");
						System.out.println();
					}
					break;
				case "5":
					System.out.println("Insert  first  operand  variable  name: A, B or C");
					poly1 = sc.nextLine();
					switch (poly1) {
					case "A":
					case "B":
					case "C":
						if (!ps.isSet(poly1.charAt(0))) {
							System.out.println("Variable not set");
						} else {
							System.out.println("Insert  second  operand  variable  name: A, B or C");
							poly2 = sc.nextLine();
							switch (poly2) {
							case "A":
							case "B":
							case "C":
								if (!ps.isSet(poly2.charAt(0))) {
									System.out.println("Variable not set");
								} else {
									ps.multiply(poly1.charAt(0), poly2.charAt(0));
									System.out.println("Result set in R: " + ps.print('R'));
								}
								break;
							default:
								System.out.println("Unknown variable!");
								System.out.println();
							}
						}
						System.out.println();
						break;
					default:
						System.out.println("Unknown variable!");
						System.out.println();
					}
					break;
				case "6":
					System.out.println("Insert  the  variable  name: A, B, C or R");
					poly1 = sc.nextLine();
					switch (poly1) {
					case "A":
					case "B":
					case "C":
					case "R":
						if (ps.isSet(poly1.charAt(0))) {
							System.out.print("Enter point: ");
							float point = Float.parseFloat(sc.nextLine());
							System.out.println("Evaluation result: " + ps.evaluatePolynomial(poly1.charAt(0), point));
						} else {
							System.out.println("Variable not set");
						}
						System.out.println();
						break;
					default:
						System.out.println("Unknown variable!");
						System.out.println();
					}
					break;
				case "7":
					System.out.println("Insert  the  variable  name: A, B, C or R");
					poly1 = sc.nextLine();
					switch (poly1) {
					case "A":
					case "B":
					case "C":
					case "R":
						if (ps.isSet(poly1.charAt(0))) {
							ps.clearPolynomial(poly1.charAt(0));
							System.out.println("Polynomial " + poly1.charAt(0) + " cleared");
						} else {
							System.out.println("Variable not set");
						}
						System.out.println();
						break;
					default:
						System.out.println("Unknown variable!");
						System.out.println();
					}
					break;
				case "exit":
					break;
				default:
					System.out.println("Invalid input!");
					System.out.println();
				}
			} catch (Exception ex) {
				System.out.println("ERROR:");
				System.out.println(ex.toString());
			}
		} while (!choice.equals("exit"));
		sc.close();
	}
}
