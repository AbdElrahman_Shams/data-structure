package eg.edu.alexu.csd.datastructure.linkedList.cs34_40;

public class SLNode {
	
	public Object data;
	public SLNode next;
	
	public SLNode(Object d, SLNode n){
		this.data = d;
		this.next = n;
	}
}