package eg.edu.alexu.csd.datastructure.linkedList.cs34_40;

import eg.edu.alexu.csd.datastructure.linkedList.*;
import java.util.HashMap;

public class PolynomialSolver implements IPolynomialSolver {

	private static class Term {

		public Term() {

		}

		public Term(int c, int e) {
			coefficient = c;
			exponent = e;
		}

		public int coefficient;
		public int exponent;
	}

	private SinglyLinkedList polyA = new SinglyLinkedList();
	private SinglyLinkedList polyB = new SinglyLinkedList();
	private SinglyLinkedList polyC = new SinglyLinkedList();
	private SinglyLinkedList polyR = new SinglyLinkedList();
	private HashMap<Character, Boolean> isSet = new HashMap<Character, Boolean>();

	public PolynomialSolver() {
		isSet.put('A', false);
		isSet.put('B', false);
		isSet.put('C', false);
		isSet.put('R', false);
	}

	public boolean isSet(char poly) {
		return isSet.get(poly);
	}

	public void setPolynomial(char poly, int[][] terms) {

		for (int i = 0; i < terms.length - 1; ++i) {
			if (terms[i][1] < terms[i + 1][1]) {
				throw new RuntimeException("Unordered input!");
			}
		}

		SinglyLinkedList selectedPoly = null;

		switch (poly) {
		case 'A':
			selectedPoly = polyA;
			break;
		case 'B':
			selectedPoly = polyB;
			break;
		case 'C':
			selectedPoly = polyC;
			break;
		default:
			throw new RuntimeException("Invalid input!");
		}

		selectedPoly.clear();
		isSet.put(poly, true);

		for (int i = 0; i < terms.length; ++i) {
			if (terms[i][1] < 0) {
				throw new RuntimeException("Negative exponents are not allowed!");
			}
			if (terms[i][0] != 0) {
				selectedPoly.add(new Term(terms[i][0], terms[i][1]));
			}
		}
	}

	// return null in case of error
	public String print(char poly) {

		StringBuilder polynomial = new StringBuilder();
		SinglyLinkedList selectedPoly = null;

		switch (poly) {
		case 'A':
			selectedPoly = polyA;
			break;
		case 'B':
			selectedPoly = polyB;
			break;
		case 'C':
			selectedPoly = polyC;
			break;
		case 'R':
			selectedPoly = polyR;
			break;
		default:
			return null;
		}

		if (isSet.get(poly) == false) {
			return null;
		} else {
			if (selectedPoly.size() == 0) {
				return "0";
			}
			Term current = new Term();
			for (int i = 0; i < selectedPoly.size(); ++i) {
				current = (Term) selectedPoly.get(i);

				if (i > 0 && current.coefficient > 0) {
					polynomial.append("+");
				}

				if (current.exponent == 0) {
					polynomial.append(current.coefficient);
				} else if (current.coefficient != 1) {
					if (current.coefficient == -1) {
						polynomial.append("-");
					} else {
						polynomial.append(current.coefficient);
					}
				}

				if (current.exponent != 0) {
					polynomial.append("x");
					if (current.exponent != 1) {
						polynomial.append("^");
						polynomial.append(current.exponent);
					}
				}
			}
		}

		return polynomial.toString();
	}

	// do nothing and return in case of error
	public void clearPolynomial(char poly) {
		switch (poly) {
		case 'A':
			polyA.clear();
			break;
		case 'B':
			polyB.clear();
			break;
		case 'C':
			polyC.clear();
			break;
		case 'R':
			polyR.clear();
			break;
		default:
			return;
		}

		isSet.put(poly, false);
	}

	public float evaluatePolynomial(char poly, float value) {
		SinglyLinkedList selectedPoly = null;
		switch (poly) {
		case 'A':
			selectedPoly = polyA;
			break;
		case 'B':
			selectedPoly = polyB;
			break;
		case 'C':
			selectedPoly = polyC;
			break;
		case 'R':
			selectedPoly = polyR;
			break;
		default:
			throw new RuntimeException("Invalid input!");
		}

		if (isSet.get(poly) == false) {
			throw new RuntimeException("Unset polynomial!");
		}

		float result = 0.0f;
		Term current = null;
		for (int i = 0; i < selectedPoly.size(); ++i) {
			current = (Term) selectedPoly.get(i);
			if (current.exponent == 0) {
				result += current.coefficient;
			} else {
				result += current.coefficient * Math.pow(value, current.exponent);
			}
		}

		return result;
	}

	public int[][] add(char poly1, char poly2) {
		SinglyLinkedList poly1List = null;
		SinglyLinkedList poly2List = null;

		switch (poly1) {
		case 'A':
			poly1List = polyA;
			break;
		case 'B':
			poly1List = polyB;
			break;
		case 'C':
			poly1List = polyC;
			break;
		default:
			throw new RuntimeException("Invalid input!");
		}

		switch (poly2) {
		case 'A':
			poly2List = polyA;
			break;
		case 'B':
			poly2List = polyB;
			break;
		case 'C':
			poly2List = polyC;
			break;
		default:
			throw new RuntimeException("Invalid input!");
		}

		if (isSet.get(poly1) == false || isSet.get(poly2) == false) {
			throw new RuntimeException("Unset polynomial!");
		}

		Term current1Term = null;
		Term current2Term = null;
		int current1 = 0;
		int current2 = 0;

		polyR.clear();
		isSet.put('R', true);

		while (current1 < poly1List.size() && current2 < poly2List.size()) {
			current1Term = (Term) poly1List.get(current1);
			current2Term = (Term) poly2List.get(current2);

			if (current1Term.exponent > current2Term.exponent) {
				polyR.add(new Term(current1Term.coefficient, current1Term.exponent));
				current1++;
			} else if (current1Term.exponent < current2Term.exponent) {
				polyR.add(new Term(current2Term.coefficient, current2Term.exponent));
				current2++;
			} else {
				Term result = new Term(current1Term.coefficient + current2Term.coefficient, current1Term.exponent);
				if (result.coefficient != 0) {
					polyR.add(result);
				}
				current1++;
				current2++;
			}
		}

		while (current1 < poly1List.size()) {
			current1Term = (Term) poly1List.get(current1);
			polyR.add(new Term(current1Term.coefficient, current1Term.exponent));
			current1++;
		}

		while (current2 < poly2List.size()) {
			current2Term = (Term) poly2List.get(current2);
			polyR.add(new Term(current2Term.coefficient, current2Term.exponent));
			current2++;
		}

		// Result of addition is zero
		if (polyR.size() == 0) {
			return new int[][] { { 0, 0 } };
		}

		Term current = null;
		int[][] results = new int[polyR.size()][2];
		for (int i = 0; i < polyR.size(); ++i) {
			current = (Term) polyR.get(i);
			results[i][0] = current.coefficient;
			results[i][1] = current.exponent;
		}

		return results;
	}

	public int[][] subtract(char poly1, char poly2) {
		SinglyLinkedList poly1List = null;
		SinglyLinkedList poly2List = null;

		switch (poly1) {
		case 'A':
			poly1List = polyA;
			break;
		case 'B':
			poly1List = polyB;
			break;
		case 'C':
			poly1List = polyC;
			break;
		default:
			throw new RuntimeException("Invalid input!");
		}

		switch (poly2) {
		case 'A':
			poly2List = polyA;
			break;
		case 'B':
			poly2List = polyB;
			break;
		case 'C':
			poly2List = polyC;
			break;
		default:
			throw new RuntimeException("Invalid input!");
		}

		if (isSet.get(poly1) == false || isSet.get(poly2) == false) {
			throw new RuntimeException("Unset polynomial!");
		}

		Term current1Term = null;
		Term current2Term = null;
		int current1 = 0;
		int current2 = 0;

		polyR.clear();
		isSet.put('R', true);

		while (current1 < poly1List.size() && current2 < poly2List.size()) {
			current1Term = (Term) poly1List.get(current1);
			current2Term = (Term) poly2List.get(current2);

			if (current1Term.exponent > current2Term.exponent) {
				polyR.add(new Term(current1Term.coefficient, current1Term.exponent));
				current1++;
			} else if (current1Term.exponent < current2Term.exponent) {
				polyR.add(new Term(-current2Term.coefficient, current2Term.exponent));
				current2++;
			} else {
				Term result = new Term(current1Term.coefficient - current2Term.coefficient, current1Term.exponent);
				if (result.coefficient != 0) {
					polyR.add(result);
				}
				current1++;
				current2++;
			}
		}

		while (current1 < poly1List.size()) {
			current1Term = (Term) poly1List.get(current1);
			polyR.add(new Term(current1Term.coefficient, current1Term.exponent));
			current1++;
		}

		while (current2 < poly2List.size()) {
			current2Term = (Term) poly2List.get(current2);
			polyR.add(new Term(-current2Term.coefficient, current2Term.exponent));
			current2++;
		}

		// Result of subtraction is zero
		if (polyR.size() == 0) {
			return new int[][] { { 0, 0 } };
		}

		Term current = null;
		int[][] results = new int[polyR.size()][2];
		for (int i = 0; i < polyR.size(); ++i) {
			current = (Term) polyR.get(i);
			results[i][0] = current.coefficient;
			results[i][1] = current.exponent;
		}

		return results;
	}

	public int[][] multiply(char poly1, char poly2) {
		SinglyLinkedList poly1List = null;
		SinglyLinkedList poly2List = null;

		switch (poly1) {
		case 'A':
			poly1List = polyA;
			break;
		case 'B':
			poly1List = polyB;
			break;
		case 'C':
			poly1List = polyC;
			break;
		default:
			throw new RuntimeException("Invalid input!");
		}

		switch (poly2) {
		case 'A':
			poly2List = polyA;
			break;
		case 'B':
			poly2List = polyB;
			break;
		case 'C':
			poly2List = polyC;
			break;
		default:
			throw new RuntimeException("Invalid input!");
		}

		if (isSet.get(poly1) == false || isSet.get(poly2) == false) {
			throw new RuntimeException("Unset polynomial!");
		}

		polyR.clear();
		isSet.put('R', true);

		// Multiplication by zero
		if (poly1List.size() == 0 || poly2List.size() == 0) {
			return new int[][] { { 0, 0 } };
		}

		Term current1Term = null;
		Term current2Term = null;
		Term currentR = null;
		Term result = null;

		for (int current1 = 0; current1 < poly1List.size(); ++current1) {
			current1Term = (Term) poly1List.get(current1);
			for (int current2 = 0; current2 < poly2List.size(); ++current2) {
				current2Term = (Term) poly2List.get(current2);
				result = new Term(current1Term.coefficient * current2Term.coefficient,
						current1Term.exponent + current2Term.exponent);
				if (result.coefficient != 0) {
					int i = 0;
					for (; i < polyR.size(); ++i) {
						currentR = (Term) polyR.get(i);
						if (currentR.exponent == result.exponent) {
							result.coefficient += currentR.coefficient;
							polyR.set(i, result);
							break;
						} else if (currentR.exponent < result.exponent) {
							polyR.add(i, result);
							break;
						}
					}

					if (i == polyR.size()) {
						polyR.add(result);
					}
				}
			}
		}

		Term current = null;
		int[][] results = new int[polyR.size()][2];
		for (int i = 0; i < polyR.size(); ++i) {
			current = (Term) polyR.get(i);
			results[i][0] = current.coefficient;
			results[i][1] = current.exponent;
		}

		return results;

	}
}
