package eg.edu.alexu.csd.datastructure.linkedList.cs34_40;

public class DLNode {
	
	public Object data;
	public DLNode next;
	public DLNode prev;
	
	public DLNode(Object d, DLNode p, DLNode n) {
		this.data = d;
		this.prev = p;
		this.next = n;
	}
}