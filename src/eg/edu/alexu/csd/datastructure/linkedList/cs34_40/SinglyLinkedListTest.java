package eg.edu.alexu.csd.datastructure.linkedList.cs34_40;

import org.junit.*;
import static org.junit.Assert.*;

public class SinglyLinkedListTest {

	@Test
	public final void testAdd() {
		// Addition to empty list
		SinglyLinkedList list = new SinglyLinkedList();
		list.add(0, new Integer(1));
		assertEquals(1, ((Integer) list.get(0)).intValue());

		list = new SinglyLinkedList();
		list.add(new Integer(1));
		assertEquals(1, ((Integer) list.get(0)).intValue());

		// Adding more elements
		list.add(new Integer(2));
		list.add(2, new Integer(3));
		list.add(new Integer(4));
		list.add(new Integer(5));
		list.add(5, new Integer(6));

		assertEquals(2, ((Integer) list.get(1)).intValue());
		assertEquals(3, ((Integer) list.get(2)).intValue());
		assertEquals(4, ((Integer) list.get(3)).intValue());
		assertEquals(5, ((Integer) list.get(4)).intValue());
		assertEquals(6, ((Integer) list.get(5)).intValue());
		assertEquals(true, list.contains(new Integer(6)));

		list.add(3, new Integer(40));
		assertEquals(3, ((Integer) list.get(2)).intValue());
		assertEquals(40, ((Integer) list.get(3)).intValue());
		assertEquals(4, ((Integer) list.get(4)).intValue());
		assertEquals(5, ((Integer) list.get(5)).intValue());
		assertEquals(6, ((Integer) list.get(6)).intValue());
		assertEquals(7, list.size());

		list.add(0, new Integer(0));
		assertEquals(0, ((Integer) list.get(0)).intValue());
	}

	@Test
	public final void testGet() {
		SinglyLinkedList list = new SinglyLinkedList();
		list.add(new Integer(1));
		list.add(new Integer(2));
		list.add(new Integer(3));
		list.add(new Integer(4));
		list.add(new Integer(5));

		assertEquals(1, ((Integer) list.get(0)).intValue());
		assertEquals(2, ((Integer) list.get(1)).intValue());
		assertEquals(3, ((Integer) list.get(2)).intValue());
		assertEquals(4, ((Integer) list.get(3)).intValue());
		assertEquals(5, ((Integer) list.get(4)).intValue());
	}

	@Test
	public final void testSet() {
		SinglyLinkedList list = new SinglyLinkedList();
		list.add(new Integer(5));
		list.add(new Integer(4));
		list.add(new Integer(3));
		list.add(new Integer(2));
		list.add(new Integer(1));

		list.set(0, new Integer(1));
		list.set(1, new Integer(2));
		list.set(2, new Integer(3));
		list.set(3, new Integer(4));
		list.set(4, new Integer(5));

		assertEquals(1, ((Integer) list.get(0)).intValue());
		assertEquals(2, ((Integer) list.get(1)).intValue());
		assertEquals(3, ((Integer) list.get(2)).intValue());
		assertEquals(4, ((Integer) list.get(3)).intValue());
		assertEquals(5, ((Integer) list.get(4)).intValue());
	}

	@Test
	public final void testClear() {
		SinglyLinkedList list = new SinglyLinkedList();
		list.add(new Integer(1));
		list.add(new Integer(2));
		list.add(new Integer(3));
		list.add(new Integer(4));
		list.add(new Integer(5));
		list.clear();
		list.add(new Integer(10));
		assertEquals(10, ((Integer) list.get(0)).intValue());
	}

	@Test
	public final void testIsEmpty() {
		SinglyLinkedList list = new SinglyLinkedList();
		assertEquals(true, list.isEmpty());

		list.add(new Integer(1));
		assertEquals(false, list.isEmpty());

		list.add(new Integer(2));
		assertEquals(false, list.isEmpty());

		list.add(new Integer(3));
		assertEquals(false, list.isEmpty());

		list.clear();
		assertEquals(true, list.isEmpty());

		list.add(new Integer(1));
		assertEquals(false, list.isEmpty());
	}

	@Test
	public final void testRemove() {
		SinglyLinkedList list = new SinglyLinkedList();
		list.add(new Integer(1));
		list.add(new Integer(2));
		list.add(new Integer(3));

		list.remove(1);
		assertEquals(1, ((Integer) list.get(0)).intValue());
		assertEquals(3, ((Integer) list.get(1)).intValue());

		list.remove(0);
		assertEquals(3, ((Integer) list.get(0)).intValue());

		list.remove(0);
		assertEquals(true, list.isEmpty());
	}

	@Test
	public final void testSize() {
		SinglyLinkedList list = new SinglyLinkedList();
		assertEquals(0, list.size());

		list.add(new Integer(1));
		assertEquals(1, list.size());

		list.add(new Integer(2));
		assertEquals(2, list.size());

		list.add(new Integer(3));
		assertEquals(3, list.size());

		list.clear();
		assertEquals(0, list.size());

		list.add(new Integer(1));
		assertEquals(1, list.size());
	}

	@Test
	public final void testSublist() {
		SinglyLinkedList list = new SinglyLinkedList();
		list.add(new Integer(1));
		list.add(new Integer(2));
		list.add(new Integer(3));
		list.add(new Integer(4));
		list.add(new Integer(5));
		list.add(new Integer(6));
		list.add(new Integer(7));

		SinglyLinkedList sublist = (SinglyLinkedList) list.sublist(2, 4); // 3,
																			// 4,
																			// 5
		assertEquals(3, ((Integer) sublist.get(0)).intValue());
		assertEquals(4, ((Integer) sublist.get(1)).intValue());
		assertEquals(5, ((Integer) sublist.get(2)).intValue());
		assertEquals(3, sublist.size());
		assertEquals(false, sublist.isEmpty());

		SinglyLinkedList sublist2 = (SinglyLinkedList) sublist.sublist(1, 1);
		assertEquals(1, sublist2.size());
		assertEquals(4, sublist2.get(0));

		sublist.remove(1);
		assertEquals(false, sublist.contains(4));

		sublist.clear();
		assertEquals(true, sublist.isEmpty());
	}

	@Test
	public final void testSublistOnlyOneElement() {
		SinglyLinkedList list = new SinglyLinkedList();
		list.add(new Integer(1));
		SinglyLinkedList sublist = (SinglyLinkedList) list.sublist(0, 0);
		assertEquals(true, sublist.contains(1));
		assertEquals(1, sublist.size());
	}

	@Test
	public final void testContains() {
		SinglyLinkedList list = new SinglyLinkedList();
		assertEquals(false, list.contains(new Integer(1)));

		list.add(new Integer(1));
		assertEquals(true, list.contains(new Integer(1)));

		list.add(new Integer(2));
		list.add(new Integer(3));
		list.add(new Integer(4));
		list.add(new Integer(5));

		assertEquals(true, list.contains(new Integer(4)));
		assertEquals(false, list.contains(new Integer(9)));

		list.remove(0);
		assertEquals(false, list.contains(new Integer(1)));

		list.remove(1);
		assertEquals(false, list.contains(new Integer(3)));

		list.clear();
		assertEquals(false, list.contains(new Integer(1)));
	}
}

