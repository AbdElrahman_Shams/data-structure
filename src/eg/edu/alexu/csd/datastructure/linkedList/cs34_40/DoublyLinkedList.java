package eg.edu.alexu.csd.datastructure.linkedList.cs34_40;

import eg.edu.alexu.csd.datastructure.linkedList.*;

public class DoublyLinkedList implements ILinkedList {

	private DLNode head = null;
	private DLNode tail = null;
	private int size = 0;

	public void add(int index, Object element) {
		if ((index < 0) || (index > size)) {
			throw new IndexOutOfBoundsException();
		}

		if (head == null) {
			head = tail = new DLNode(element, null, null);
			size = 1;
		}

		else if(index == 0) {
			head = new DLNode(element, null, head);
			size++;
		}

		else {
			DLNode current = this.head;
			for (int i = 0; i < index - 1; ++i) {
				current = current.next;
			}
			DLNode temp = current.next;
			current.next = new DLNode(element, current, temp);
			if (index == size) {
				tail = current.next;
			} else {
				if (index != 0) {
					temp.prev = current.next;
				}
			}
			++size;
		}
	}

	public void add(Object element) {
		if (head == null) {
			head = tail = new DLNode(element, null, null);
		} else {
			tail = tail.next = new DLNode(element, tail, null);
		}
		++size;
	}

	public Object get(int index) {
		if ((index < 0) || (index >= size)) {
			throw new IndexOutOfBoundsException();
		}

		DLNode current = head;
		for (int i = 0; i < index; ++i) {
			current = current.next;
		}

		return current.data;
	}

	public void set(int index, Object element) {
		if ((index < 0) || (index >= size)) {
			throw new IndexOutOfBoundsException();
		}

		DLNode current = head;
		for (int i = 0; i < index; ++i) {
			current = current.next;
		}

		current.data = element;
	}

	public void clear() {
		head = tail = null;
		size = 0;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public void remove(int index) {
		if ((index < 0) || (index >= size)) {
			throw new IndexOutOfBoundsException();
		}

		if (size == 1) {
			clear();
		} else {
			DLNode current = head;
			for (int i = 0; i < index - 1; ++i) {
				current = current.next;
			}
			DLNode temp = current.next;
			DLNode temp2 = temp.next;
			if (index == 0) {
				head = head.next;
				current.next = null;
				temp.prev = null;
			} else if (index == size - 1) {
				current.next = null;
				temp.prev = null;
			} else {
				current.next = temp2;
				temp2.prev = current;
				temp.next = null;
				temp.prev = null;
			}
			--size;

		}
	}

	public int size() {
		return size;
	}

	public ILinkedList sublist(int fromIndex, int toIndex) {
		if ((fromIndex < 0) || (fromIndex >= size)) {
			throw new IndexOutOfBoundsException();
		}
		if ((toIndex < 0) || (toIndex >= size)) {
			throw new IndexOutOfBoundsException();
		}
		if (fromIndex > toIndex) {
			throw new IndexOutOfBoundsException();
		}

		DoublyLinkedList sublist = new DoublyLinkedList();
		DLNode current = head;

		if (fromIndex == toIndex) {
			for (int i = 0; i < fromIndex; ++i) {
				current = current.next;
			}

			sublist.add(current.data);
			return sublist;
		}

		for (int i = 0; i <= toIndex; ++i) {
			if (i >= fromIndex) {
				sublist.add(current.data);
			}
			current = current.next;
		}
		return sublist;
	}

	public boolean contains(Object o) {
		DLNode current = head;
		if (head == null) {
			return false;
		}
		while (current != null) {
			if (current.data.equals(o)) {
				return true;
			}
			current = current.next;
		}

		return false;
	}
}
