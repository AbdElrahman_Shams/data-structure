package eg.edu.alexu.csd.datastructure.linkedList.cs34_40;

import org.junit.*;
import static org.junit.Assert.*;

public class DoublyLinkedListTest {

	@Test
	public final void testAdd() {
		// Addition to empty list
		DoublyLinkedList list = new DoublyLinkedList();
		list.add(0, new Integer(1));
		assertEquals(1, ((Integer) list.get(0)).intValue());

		list = new DoublyLinkedList();
		list.add(new Integer(1));
		assertEquals(1, ((Integer) list.get(0)).intValue());

		// Adding more elements
		list.add(new Integer(2));
		list.add(new Integer(3));
		list.add(new Integer(4));
		list.add(new Integer(5));
		list.add(5, new Integer(6));

		assertEquals(2, ((Integer) list.get(1)).intValue());
		assertEquals(3, ((Integer) list.get(2)).intValue());
		assertEquals(4, ((Integer) list.get(3)).intValue());
		assertEquals(5, ((Integer) list.get(4)).intValue());
		assertEquals(6, ((Integer) list.get(5)).intValue());
		assertEquals(true, list.contains(new Integer(6)));

		list.add(0, new Integer(0));
		assertEquals(0, ((Integer) list.get(0)).intValue());
	}

	@Test
	public final void testGet() {
		DoublyLinkedList list = new DoublyLinkedList();
		list.add(new Integer(1));
		list.add(new Integer(2));
		list.add(new Integer(3));
		list.add(new Integer(4));
		list.add(new Integer(5));

		assertEquals(1, ((Integer) list.get(0)).intValue());
		assertEquals(2, ((Integer) list.get(1)).intValue());
		assertEquals(3, ((Integer) list.get(2)).intValue());
		assertEquals(4, ((Integer) list.get(3)).intValue());
		assertEquals(5, ((Integer) list.get(4)).intValue());
	}

	@Test
	public final void testSet() {
		DoublyLinkedList list = new DoublyLinkedList();
		list.add(new Integer(5));
		list.add(new Integer(4));
		list.add(new Integer(3));
		list.add(new Integer(2));
		list.add(new Integer(1));

		list.set(0, new Integer(1));
		list.set(1, new Integer(2));
		list.set(2, new Integer(3));
		list.set(3, new Integer(4));
		list.set(4, new Integer(5));

		assertEquals(1, ((Integer) list.get(0)).intValue());
		assertEquals(2, ((Integer) list.get(1)).intValue());
		assertEquals(3, ((Integer) list.get(2)).intValue());
		assertEquals(4, ((Integer) list.get(3)).intValue());
		assertEquals(5, ((Integer) list.get(4)).intValue());
	}

	@Test
	public final void testClear() {
		DoublyLinkedList list = new DoublyLinkedList();
		list.add(new Integer(1));
		list.add(new Integer(2));
		list.add(new Integer(3));
		list.add(new Integer(4));
		list.add(new Integer(5));
		list.clear();
		list.add(new Integer(10));
		assertEquals(10, ((Integer) list.get(0)).intValue());
	}

	@Test
	public final void testIsEmpty() {
		DoublyLinkedList list = new DoublyLinkedList();
		assertEquals(true, list.isEmpty());

		list.add(new Integer(1));
		assertEquals(false, list.isEmpty());

		list.add(new Integer(2));
		assertEquals(false, list.isEmpty());

		list.add(new Integer(3));
		assertEquals(false, list.isEmpty());

		list.clear();
		assertEquals(true, list.isEmpty());

		list.add(new Integer(1));
		assertEquals(false, list.isEmpty());
	}

	@Test
	public final void testRemove() {
		DoublyLinkedList list = new DoublyLinkedList();
		list.add(new Integer(1));
		list.add(new Integer(2));
		list.add(new Integer(3));

		list.remove(1);
		assertEquals(1, ((Integer) list.get(0)).intValue());
		assertEquals(3, ((Integer) list.get(1)).intValue());

		list.remove(0);
		assertEquals(3, ((Integer) list.get(0)).intValue());

		list.remove(0);
		assertEquals(true, list.isEmpty());
	}

	@Test
	public final void testSize() {
		DoublyLinkedList list = new DoublyLinkedList();
		assertEquals(0, list.size());

		list.add(new Integer(1));
		assertEquals(1, list.size());

		list.add(new Integer(2));
		assertEquals(2, list.size());

		list.add(new Integer(3));
		assertEquals(3, list.size());

		list.clear();
		assertEquals(0, list.size());

		list.add(new Integer(1));
		assertEquals(1, list.size());
	}

	@Test
	public final void testSublist() {
		DoublyLinkedList list = new DoublyLinkedList();
		list.add(new Integer(1));
		list.add(new Integer(2));
		list.add(new Integer(3));
		list.add(new Integer(4));
		list.add(new Integer(5));
		list.add(new Integer(6));
		list.add(new Integer(7));

		DoublyLinkedList sublist = (DoublyLinkedList) list.sublist(2, 4); // 3,
																			// 4,
																			// 5
		assertEquals(3, ((Integer) sublist.get(0)).intValue());
		assertEquals(4, ((Integer) sublist.get(1)).intValue());
		assertEquals(5, ((Integer) sublist.get(2)).intValue());
		assertEquals(3, sublist.size());
		assertEquals(false, sublist.isEmpty());
		
		DoublyLinkedList sublist2 = (DoublyLinkedList) sublist.sublist(1, 1);
		assertEquals(1, sublist2.size());
		assertEquals(4, sublist2.get(0));

		sublist.remove(1);
		assertEquals(false, sublist.contains(4));

		sublist.clear();
		assertEquals(true, sublist.isEmpty());
	}

	@Test
	public final void testSublistOnlyOneElement() {
		DoublyLinkedList list = new DoublyLinkedList();
		list.add(new Integer(1));
		DoublyLinkedList sublist = (DoublyLinkedList) list.sublist(0, 0);
		assertEquals(true, sublist.contains(1));
		assertEquals(1, sublist.size());
	}

	@Test
	public final void testContains() {
		DoublyLinkedList list = new DoublyLinkedList();
		assertEquals(false, list.contains(new Integer(1)));

		list.add(new Integer(1));
		assertEquals(true, list.contains(new Integer(1)));

		list.add(new Integer(2));
		list.add(new Integer(3));
		list.add(new Integer(4));
		list.add(new Integer(5));

		assertEquals(true, list.contains(new Integer(4)));
		assertEquals(false, list.contains(new Integer(9)));

		list.remove(0);
		assertEquals(false, list.contains(new Integer(1)));

		list.remove(1);
		assertEquals(false, list.contains(new Integer(3)));

		list.clear();
		assertEquals(false, list.contains(new Integer(1)));
	}
}

